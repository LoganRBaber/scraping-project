'''
This is an attempt to scrape job websites for jobs where the keyword is python, then extract the description and count the frequency of words to determine the most sought after skills and knowledge required for python jobs

'''

import requests
from bs4 import BeautifulSoup

import collections
import time
import json

#Get job urls
def get_listing_urls(keyword):
    #Gets HTML from seek keyword search
    urls = []
    page_number = 1
    while True:
        page_urls = []
    
        response = requests.get('https://www.seek.co.nz/{}-jobs?page={}'.format(keyword, page_number))
        html = response.content
        soup = BeautifulSoup(html, 'html.parser')
        
        #Finds search results, if blank break loop
        if soup.find('div', attrs = {'data-automation': 'searchZeroResults'}) != None:
            print 'Finished searching!'
            print 'Total Urls found: {}'.format(len(urls))
            break
        
        
        #Finds all <a> tags with specific class used in seek HTML for job listing titles
        a_tags = soup.findAll('a', attrs = {'class':'_1EkZJQ7'})

        for job in a_tags:
            job = str(job)
            job = unicode(job, 'utf-8')
            url_start = job.find(u'href="') + 6
            url_end = job[url_start:-1].find('"')
            urls.append('https://www.seek.co.nz{}'.format(job[url_start:url_start+url_end]))
            page_urls.append('https://www.seek.co.nz{}'.format(job[url_start:url_start+url_end]))
    
        
        
        time.sleep(5)
        print 'Got {} urls from page number {}'.format(len(page_urls), page_number)
        page_number += 1
    
    return urls


#Get job description
def get_job_description(url):

    response = requests.get(url)
    html = response.content
    soup = BeautifulSoup(html, 'html.parser')

    job_description = soup.find('div', attrs={'class': 'templatetext'})
    if job_description == None:
        job_description = soup.find('div', attrs={'data-automation': 'mobileTemplate'})
        
    job_description_text = job_description.get_text()

    return job_description_text

#Counts occurrences of words and returns as a dictionary
def word_count(string, word_dict = {}):

    #Remove newlines and carriage returns and unicode spaces
    string = string.replace('\n',' ').replace('\r',' ').replace(u'\xa0',u' ')
    
    #Remove slashes, commas, Astrixes 
    string = string.replace(',', '').replace('/', ' ').replace('\\', ' ').replace('*',' ').replace('-',' ')
    
    #TODO: Figure out how to decode the \u characters in html
    
    #Split string by space, then remove whitespace in list
    filtered_temp = string.split(' ')
    filtered = [i for i in filtered_temp if i != '']
    
    #Create ordered dictionary for viewing
    for word in filtered:
        word = word.lower()
        if word in word_dict:
            word_dict[word] = word_dict[word] + 1
        else:
            word_dict[word] = 1
    
    ordered_word_dict = collections.OrderedDict(sorted(word_dict.items(), key=lambda i: -i[1]))
    return ordered_word_dict
            
def trim_word_dictionary(word_dictionary):
    wd = word_dictionary
    
    #Remove common words in English language (Top 100 from wikipedia)
    common_words = ['the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'I', 'it', 'for', 'not', 'on', 'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 'or', 'an', 'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which', 'go', 'me', 'when', 'make', 'can', 'like', 'time', 'no', 'just', 'him', 'know', 'take', 'people', 'into', 'year', 'your', 'good', 'some', 'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also', 'back', 'after', 'use', 'two', 'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new', 'want', 'because', 'any', 'these', 'give', 'day', 'most', 'us']
    
    for word in common_words:
        try:
            del wd[word]
        except:
            pass
    
    
    return wd
            
if __name__ == '__main__':
    
    urls = get_listing_urls('Python')
    time.sleep(3)
    
    for count, url in enumerate(urls):
        print 'Analysing url {} of {}'.format(count, len(urls))
        jd = get_job_description(url)
        try:
            wd = word_count(jd, wd)
        except:
            wd = word_count(jd, {})

        time.sleep(3)
        
    print wd
    
    '''
    fileName = 'C:\Users\lbab853\Documents\Scraper_project\data.json'
    with open('{}'.format(fileName), 'r+') as f:
    
        wd = json.load(f, object_pairs_hook=collections.OrderedDict)
    print wd
    '''
    #Trim word dictionary
    wd = trim_word_dictionary(wd)
    print wd

    fileName = 'C:\Users\lbab853\Documents\Scraper_project\data.json'
    with open('{}_trimmed'.format(fileName), 'w+') as f:
        json.dump(wd, f)

        
