The aim of this repository is to create a basic webscraper.
This is the first web scraper I have made, so I don't expect it to be really amazing.

I would like to have the following functionalitys: 

Provide a keyword, and search that keyword on seek.co.nz
Get a list of job listing urls returned by the search
Go to each job listing and extract the job description (Possibly other data too)
Return a dictionary of words and word frequency.
Be able to merge dictionaries together so that I can find overall word frequency

Would be nice to have:

Database functionality
Hosted on a webserver to run repetativley, and only update with new job listings